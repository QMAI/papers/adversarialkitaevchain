# Import libraries
import numpy as np
from numpy import mean
from numpy import std

import matplotlib.pyplot as plt
import matplotlib

import tensorflow as tf
from tensorflow.keras.models import Sequential
from tensorflow.keras.models import load_model
from tensorflow.keras.layers import Conv2D
from tensorflow.keras.layers import MaxPooling2D
from tensorflow.keras.layers import Dense
from tensorflow.keras.layers import Flatten
from tensorflow.keras.optimizers import SGD
from sklearn.preprocessing import MinMaxScaler


class CNN():
    """ Class for building a Convolutional Neural Network and the
        application for many-body systems."""
        
    def __init__(self):
        # Inputs 
        self.pixel_x = 28 # number of pixel in x-direction
        self.pixel_y = 28 # number of pixel in y-direction
        self.pixels = 28 
        self.input_shape = (self.pixel_x,self.pixel_y,1)
        
        # parameter for visualization
        self.xs = np.linspace(-10.0,10,self.pixel_x) 
        self.ys = np.linspace(-10.0,10,self.pixel_y) 

        # initialize CNN
        self.model = self.CNN_network()
                
              
    def CNN_network(self):
        """ Building the CNN network """       
        # build the layers
        model = tf.keras.models.Sequential([
            tf.keras.layers.Conv2D(32, (5,5), padding='same', activation='relu', input_shape=self.input_shape),
            tf.keras.layers.Conv2D(32, (5,5), padding='same', activation='relu'),
            tf.keras.layers.MaxPool2D(),
            tf.keras.layers.Dropout(0.25),
            tf.keras.layers.Conv2D(64, (3,3), padding='same', activation='relu'),
            tf.keras.layers.Conv2D(64, (3,3), padding='same', activation='relu'),
            tf.keras.layers.MaxPool2D(strides=(2,2)),
            tf.keras.layers.Dropout(0.25),
            tf.keras.layers.Flatten(),
            tf.keras.layers.Dense(128, activation='relu'),
            tf.keras.layers.Dropout(0.5),
            tf.keras.layers.Dense(1, activation='sigmoid')
        ])        
        # compile the model 
        model.compile(optimizer=tf.keras.optimizers.RMSprop(epsilon=1e-08), loss='mean_squared_error',
                      metrics=[tf.keras.metrics.MeanSquaredError()])       
        return model
    
    
    def training(self, x_train, l_train, batch_size=64, epochs=5, validation_split=0.1):
        """ train the CNN """
        history = self.model.fit(x_train, l_train, batch_size=batch_size, epochs=epochs, validation_split=validation_split)
        return history


    def create_3D_plot(self, spectra):
        """ create 3D plot """
        matplotlib.rcParams['font.family'] = "Bitstream Vera Serif"
        fig = plt.figure(dpi=100)
        fig.subplots_adjust(0.2,0.2)
        plt.contourf(self.xs, self.ys, spectra, 100)
        plt.ylabel("muR")
        plt.xlabel("muL")
        plt.show()   
        return None


    def format_inputs(self, data, label, val_split=0.1):
        "function to import and (pre) process dataset"      
        # use MinMaxScaler for data and 
        scaler = MinMaxScaler(feature_range=(0,1)) # data
        scaler_1 = MinMaxScaler(feature_range=(0,1)) # label 1
        
        n_samples = len(data) #number of samples in the dataset     
        spectra_format = np.zeros((n_samples,self.pixels*self.pixels))
        
        # 3D to 2D for scaling    
        for k in range(n_samples):    
            for m in range(self.pixels):
                for l in range(self.pixels):
                    spectra_format[k,m*self.pixels+l] = data[k,l,m]
                              
        # test and training data
        x_train = scaler.fit_transform(spectra_format[:int((1-val_split)*n_samples),:]) 
        x_test = scaler.transform(spectra_format[int((1-val_split)*n_samples):,:])
    
        # test and training labels            
        labels = label            
        l_train = scaler_1.fit_transform(labels[:int((1-val_split)*n_samples),0:1])     
        l_test = scaler_1.transform(labels[int((1-val_split)*n_samples):,0:1]) 
        
        # back to 28x28 format for training/plotting
        x_train_CNN = np.zeros((int((1-val_split)*n_samples), self.pixels, self.pixels))
        for m in range(int((1-val_split)*n_samples)):
            for j in range(self.pixels):
                for i in range(self.pixels):
                    x_train_CNN[m,i,j]=x_train[m,j*self.pixels+i]        
        x_train_CNN = x_train_CNN.reshape((x_train_CNN.shape[0], 28, 28, 1))
        
        # test data
        x_test_CNN = np.zeros((int((val_split)*n_samples), self.pixels, self.pixels))
        for m in range(int((val_split)*n_samples)):
            for j in range(self.pixels):
                for i in range(self.pixels):
                    x_test_CNN[m,i,j]=x_test[m,j*self.pixels+i]       
        x_test_CNN_input = x_test_CNN.reshape((x_test_CNN.shape[0], 28, 28, 1))
 
        return x_train_CNN, x_test_CNN_input, l_train, l_test, scaler, scaler_1
    

    def cut_image(self, data_exp, x_shift, y_shift, high_res=51, show_images=True):
        """reduce (experimental) image size to 28x28 pixels.
            Shift center position of cut:
            x_shift: bigger -> left
            y_shift: bigger -> down"""
        # initialize data arrays
        data_cut = np.zeros((1,self.pixels,self.pixels,1))        
        data_exp_3D = np.zeros((high_res,high_res))
        
        for j in range(high_res):
            for i in range(high_res):
                data_exp_3D[j,i]=data_exp[j*high_res+i]
            
        data_cut[0,:,:,0] = data_exp_3D[(y_shift-self.pixels):y_shift, x_shift:(self.pixels+x_shift)]

        if show_images==True:
            # high resolution
            muL_vals = np.linspace(-1, 1, high_res) # on-site energies for left quantum dot
            muR_vals = np.linspace(-1, 1, high_res) # on-site energies for right quantum dot
                          
            matplotlib.rcParams['font.family'] = "Bitstream Vera Serif"
            fig = plt.figure(dpi=115, figsize=[4,4])
            plt.contourf(muL_vals,muR_vals, data_exp_3D, 100, cmap='OrRd')
            plt.ylabel("muR")
            plt.xlabel("muL")
            plt.show()
            
            # low resolution
            muL_vals_2 = np.linspace(0, 1, self.pixels) # on-site energies for left quantum dot
            muR_vals_2 = np.linspace(0, 1, self.pixels) # on-site energies for right quantum dot
                    
            matplotlib.rcParams['font.family'] = "Bitstream Vera Serif"
            fig = plt.figure(dpi=115, figsize=[4,4])
            plt.contourf(muL_vals_2,muR_vals_2, data_cut[0,:,:,0], 100, cmap='OrRd')
            plt.axvline(x=0.5,color='red', ls='--', lw=1.0)
            plt.axhline(y=0.5,color='red', ls='--', lw=1.0)
            plt.ylabel("muR")
            plt.xlabel("muL")
            plt.show()
            
            print('shift center left: increase "x_shift"')
            print('shift center down: increase "y_shift"')      
        return data_cut


    def cut_image_3D(self, data_exp, x_shift, y_shift, high_res=51, show_images=True):
        """reduce (experimental) image size to 28x28 pixels.
            Shift center position of cut:
            x_shift: bigger -> left
            y_shift: bigger -> down"""
        # initialize data arrays
        data_cut = np.zeros((1,self.pixels,self.pixels,1))        
        data_exp_3D = data_exp            
        data_cut[0,:,:,0] = data_exp_3D[(y_shift-self.pixels):y_shift, x_shift:(self.pixels+x_shift)]

        if show_images==True:
            # high resolution
            muL_vals = np.linspace(-1, 1, high_res) # on-site energies for left quantum dot
            muR_vals = np.linspace(-1, 1, high_res) # on-site energies for right quantum dot
                          
            matplotlib.rcParams['font.family'] = "Bitstream Vera Serif"
            fig = plt.figure(dpi=115, figsize=[4,4])
            plt.contourf(muL_vals,muR_vals, data_exp_3D, 100, cmap='OrRd')
            plt.ylabel("muR")
            plt.xlabel("muL")
            plt.show()
            
            # low resolution
            muL_vals_2 = np.linspace(0, 1, self.pixels) # on-site energies for left quantum dot
            muR_vals_2 = np.linspace(0, 1, self.pixels) # on-site energies for right quantum dot
                    
            matplotlib.rcParams['font.family'] = "Bitstream Vera Serif"
            fig = plt.figure(dpi=115, figsize=[4,4])
            plt.contourf(muL_vals_2,muR_vals_2, data_cut[0,:,:,0], 100, cmap='OrRd')
            plt.axvline(x=0.5,color='red', ls='--', lw=1.0)
            plt.axhline(y=0.5,color='red', ls='--', lw=1.0)
            plt.ylabel("muR")
            plt.xlabel("muL")
            plt.show()
            
            print('shift center left: increase "x_shift"')
            print('shift center down: increase "y_shift"')      
        return data_cut
    

    def experimental_data(self, spectra, label, scaler_1, plot_orig=True):
        "estimate Hamiltonian parameters of experimental measurement"                    
        if plot_orig == True:
            self.create_3D_plot(spectra[0,:,:,0])         
        pred_exp = self.model.predict(spectra)    
        pred_exp_scaled = scaler_1.inverse_transform(pred_exp)
        print("prediction:", pred_exp_scaled[0,0])
        print("measurement:" , label)   
        return None   


    def experimental_data_max_pred(self, spectra, label, scaler_1, plot_orig=True):
        "estimate Hamiltonian parameters of experimental measurement"                    
        if plot_orig == True:
            self.create_3D_plot(spectra[0,:,:,0])         
        pred_exp = self.model.predict(spectra)    
        pred_exp_scaled = scaler_1.inverse_transform(pred_exp)  
        return pred_exp_scaled[0,0]   


    def scale_exp_data(self, data_exp, scale_max=0.85, scale_grad=1.0):
        """ scale experimental measurements to match training data.
            scale_max: changes maximum intensity of the image 
            scale_grad: changes gradient of intensity decay ~ noise level """
        return data_exp[:,:,:,:]**scale_grad * (1/np.amax(data_exp)**scale_grad) * scale_max    
            
        
    def fit_scaler(self, scaler, data):
        """ fit only one specific scaler """
        scaler.fit(data)
        return scaler 
    
    
    def set_scaler(self):
        """ initialize scaler and scaler_1 """
        scaler = MinMaxScaler(feature_range=(-0,1)) # data
        scaler_1 = MinMaxScaler(feature_range=(0,1)) # label 1
        return scaler, scaler_1
   

    def save_model(self,model):
        """ save the model and weights """ 
        self.model.save('data_CNN/CNN_delta.h5')
        return None
 
    
    def load_model(self, model):
        """ save the model and weights """
        model = self.model.load_weights('data_CNN/CNN_delta_new.h5')
        return None
    
  