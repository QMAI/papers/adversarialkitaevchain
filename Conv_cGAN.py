# Conv-cGAN library

from __future__ import print_function, division

# import libraries and packages
import os
os.environ["KERAS_BACKEND"] = "tensorflow"
import tensorflow as tf
from keras.layers import LeakyReLU, Activation, Input, Dense, Dropout, Concatenate, BatchNormalization, GaussianNoise
from keras.models import Model,Sequential
from keras.regularizers import l1_l2
from tensorflow.keras.optimizers import Adam, SGD
from keras import activations
from tensorflow.keras.models import load_model

from keras import backend as K
from keras.layers import Input, Dense, Reshape, Flatten, Concatenate, UpSampling2D,MaxPool2D
from keras.layers import BatchNormalization, Activation, Embedding, multiply
from keras.layers.advanced_activations import LeakyReLU
from keras.layers.convolutional import Conv2D, Conv2DTranspose

# check if GPU is used
tf.config.list_physical_devices('GPU')

# other  libraries
import matplotlib.pyplot as plt
import matplotlib
import numpy as np
import sys
import seaborn as sns
from sklearn.preprocessing import MinMaxScaler



class CGAN_CNN():
    """ Class for conditional generative adversarial networks (cGANS) 
        using Convolutional Neural Networks (CNNs)."""
        
    def __init__(self):
        # Inputs 
        self.conditions = 1 # number of conditional parameters
        self.pixels = 28
        self.input_dim = self.pixels * self.pixels
        self.input_shape = (self.pixels, self.pixels)
        self.latent_dim = 100
        
        # parameter for visualization
        self.xs = np.linspace(-10.0,10.0,self.pixels) # x-axis ~ onsite energy 1
        self.ys = np.linspace(-10.0,10.0,self.pixels) # y-axis ~ onsite energy 2
        
        optimizer_gan = Adam(0.00001, 0.5) # optimizer for generator and cGAN total   
        
        self.regulizer = lambda: l1_l2(1e-5, 1e-5) #kenel-regulizer for both networks
        
        # Build and compile the discriminator
        self.discriminator = self.build_discriminator()
        self.discriminator.compile(loss=['binary_crossentropy'],
            optimizer=optimizer_gan) 
           
        # Build and compile the generator
        self.generator = self.build_generator()
        self.generator.compile(loss=['mean_squared_error'],
            optimizer=optimizer_gan) 
        
        # Build the cGAN
        self.discriminator.trainable = False
        self.cgan = self.build_cgan()
        self.cgan.compile(loss='binary_crossentropy', optimizer=optimizer_gan)
        
        
        
    def build_generator(self):
        # generator   
        z      = (Input(shape=(self.latent_dim,)))
        label  = (Input(shape=(self.conditions,)))
        inputs = (Concatenate())([z, label])       
        # layers
        g = (Dense(128*7*7, activation = 'relu'))(inputs)
        g = (Reshape((7,7,128)))(g)
        g = (Conv2DTranspose(128, (3,3), strides=(2, 2), padding='same', activation='relu'))(g)
        g = (Dropout(0.25))(g)
        g = (Conv2DTranspose(64, (5,5), strides=(2, 2), padding='same', activation='relu'))(g)
        g = (Dropout(0.25))(g)
        g = (Conv2D(1, (7,7), activation='sigmoid', padding='same'))(g)
        model = Model(inputs=[z, label], outputs=[g, label], name="generator")
        return model
    
   
    
    def build_discriminator(self):
        # discriminator    
        x      = (Input(shape=(self.pixels,self.pixels,1)))
        label  = (Input(shape=(self.conditions,)))    
        # layers
        d = (Conv2D(64, (5,5), padding='same', activation='relu', input_shape=(self.pixels,self.pixels,1)))(x)
        d = (MaxPool2D())(d)
        d = (Dropout(0.25))(d)
        d = (Conv2D(128, (3,3), padding='same', activation='relu'))(d)
        d = (MaxPool2D(strides=(2,2)))(d)
        d = (Dropout(0.25))(d)
        d = (Flatten())(d)
        d = (Concatenate())([d, label])
        d = (Dense(128, activation='relu'))(d)
        d = (Dense(1))(d)
        d = (Activation('sigmoid'))(d)        
        model = Model(inputs=[x, label], outputs=d, name="discriminator")
        return model
    
    
    
    def build_cgan(self):
        # building the cGAN
        yfake = Activation("linear", name="yfake")(self.discriminator(self.generator(self.generator.inputs)))
        yreal = Activation("linear", name="yreal")(self.discriminator(self.discriminator.inputs))
        model = Model(self.generator.inputs + self.discriminator.inputs, [yfake, yreal], name="cGAN")
        return model



    def train(self, x_train, l_train, epochs=30, batch_size=32):
        " function for the training of the cGAN (can be quite challenging)"
        # training parameter
        num_batches = int(x_train.shape[0] / batch_size)
        # training loop
        for epoch in range(epochs):            
            print("Epoch {}/{}".format(epoch + 1, epochs))
            for index in range(num_batches):                
                # train discriminator
                self.discriminator.trainable = True
            
                # train discriminator on real data
                batch       = np.random.randint(0, x_train.shape[0], size=batch_size)
                input_batch = x_train[batch]
                label_batch = l_train[batch]
                y_real      = np.ones(batch_size) + 0.1 * np.random.uniform(-1, 1, size=batch_size)
                self.discriminator.train_on_batch([input_batch, label_batch], y_real)
                
                # train discriminator on fake data
                noise_batch      = np.random.normal(0, 1, (batch_size, self.latent_dim))    
                generated_images = self.generator.predict([noise_batch, label_batch])
                y_fake           = np.zeros(batch_size) + 0.1 * np.random.uniform(0, 1, size=batch_size)
                d_loss = self.discriminator.train_on_batch(generated_images, y_fake)  
                self.discriminator.trainable = False
                
                # train GAN
                gan_loss = self.cgan.train_on_batch([noise_batch, label_batch, input_batch, label_batch], [y_real, y_fake])
                print("Batch {}/{}: Discriminator loss = {}, GAN loss = {}".format(index + 1, num_batches, d_loss, gan_loss))
                return None
         
    
    
    def save_weights_delta(self, cGAN):
        "function to load trained model"                
        self.generator.save_weights('data_cGAN/generator_CNN_all_new.h5')
        self.discriminator.save_weights('data_cGAN/discriminator_CNN_all_new.h5')
        self.cgan.save_weights('data_cGAN/gan_CNN_all_new.h5')          
        return cGAN   
 
    
 
    def load_weights_delta(self, cGAN):
        "function to load trained model"               
        self.generator.load_weights('data_cGAN/generator_CNN_all.h5')
        self.discriminator.load_weights('data_cGAN/discriminator_CNN_all.h5')
        self.cgan.load_weights('data_cGAN/gan_CNN_all.h5')             
        return cGAN   
    
    
    
    def load_weights_uniform(self, cGAN):
        "function to load trained model"               
        self.generator.load_weights('data_cGAN/generator_CNN_uniform_data.h5')
        self.discriminator.load_weights('data_cGAN/discriminator_CNN_uniform_data.h5')
        self.cgan.load_weights('data_cGAN/gan_CNN_uniform_data.h5')             
        return cGAN  
    
      
       
    def format_inputs(self, data, label, val_split=0.1):
        "function to import and (pre) process dataset"        
        # use MinMaxScaler for data and label
        scaler = MinMaxScaler(feature_range=(-1,1)) # data
        scaler_1 = MinMaxScaler(feature_range=(0,1)) # label 1
        
        n_samples = len(data) #number of samples in the dataset     
        spectra_format = np.zeros((n_samples,self.pixels*self.pixels))
        
        # 3D to 2D for scaling    
        for k in range(n_samples):    
            for m in range(self.pixels):
                for l in range(self.pixels):
                    spectra_format[k,m*self.pixels+l] = data[k,l,m]
                              
        # test and training data
        x_train = scaler.fit_transform(spectra_format[:int((1-val_split)*n_samples),:]) 
        x_test = scaler.transform(spectra_format[int((1-val_split)*n_samples):,:])
    
        # test and training labels            
        labels = label            
        l_train = scaler_1.fit_transform(labels[:int((1-val_split)*n_samples),0:1])     
        l_test = scaler_1.transform(labels[int((1-val_split)*n_samples):,0:1]) 
        
        # back to 28x28 format for training/plotting
        x_train_CNN = np.zeros((int((1-val_split)*n_samples), self.pixels, self.pixels))
        for m in range(int((1-val_split)*n_samples)):
            for j in range(self.pixels):
                for i in range(self.pixels):
                    x_train_CNN[m,i,j]=x_train[m,j*self.pixels+i]        
        x_train_CNN = x_train_CNN.reshape((x_train_CNN.shape[0], 28, 28, 1))
        
        # test data
        x_test_CNN = np.zeros((int((val_split)*n_samples), self.pixels, self.pixels))
        for m in range(int((val_split)*n_samples)):
            for j in range(self.pixels):
                for i in range(self.pixels):
                    x_test_CNN[m,i,j]=x_test[m,j*self.pixels+i]       
        x_test_CNN_input = x_test_CNN.reshape((x_test_CNN.shape[0], 28, 28, 1))
 
        return x_train_CNN, x_test_CNN_input, l_train, l_test, scaler, scaler_1
    
    
    
    def generate_G(self, param_1, scaler, scaler_1, batch_size=128):
        """ function to generate new samples.
            insert conditional parameter, scaled in the interval I=[0,1]"""        
        # initilize sampling 
        noise_batch = np.random.normal(0, 1, (batch_size, self.latent_dim)) 
        noise_batch[0] = np.random.uniform(0, 1, (1, self.latent_dim))
        generate_batch = np.zeros((batch_size,1))
        
        # conditional parameter
        generate_batch[0,0] = param_1 # insert here N_y scaled in [0.0, 1.0]  
        
        # make new prediction
        pred = self.generator.predict([noise_batch, generate_batch])[0]     
        pred_2D = self.data_3D_to_2D(pred[0:1,:,:,0])
        pred_2D = scaler_1.inverse_transform(pred_2D)
        pred_3D = self.data_2D_to_3D(pred_2D)
        self.create_3D_plot(pred_3D[0,:,:])
        
        # print real conditional parameter values
        param_1_rescaled = scaler_1.inverse_transform(generate_batch)
        print('conditional parameter (t=1):')
        print('Delta/t = ',  param_1_rescaled[0,0])        
        return None  



    def generate_G_return(self, param_1, scaler, scaler_1, batch_size=128):
        """ function to generate new samples.
            insert conditional parameter, scaled in the interval I=[0,1]"""        
        # initilize sampling 
        noise_batch = np.random.normal(0, 1, (batch_size, self.latent_dim)) 
        noise_batch[0] = np.random.uniform(0, 1, (1, self.latent_dim))
        generate_batch = np.zeros((batch_size,1))
        
        # conditional parameter
        generate_batch[0,0] = param_1 # insert here N_y scaled in [0.0, 1.0]  
        
        # make new prediction
        pred = self.generator.predict([noise_batch, generate_batch])[0]     
        pred_2D = self.data_3D_to_2D(pred[0:1,:,:,0])
        pred_2D = scaler_1.inverse_transform(pred_2D)
        pred_3D = self.data_2D_to_3D(pred_2D)
        param_1_rescaled = scaler_1.inverse_transform(generate_batch)
      
        return pred_3D[0,:,:], param_1_rescaled[0,0]
    
        
    
    def param_estimation(self, spectra, label, scaler_1, n_plot=0, x_points=25, batch_size=128):
        """ estimate Hamiltonian parameters (here: Delta/t ratio) """        
        # initialize
        d_params = np.zeros(x_points)
        batch = np.random.randint(0, spectra.shape[0], size=batch_size)
        image_batch_test = spectra[batch]
        label_batch_test = label[batch]
        
        # feed new data
        n_plot = n_plot
        test_label_batch = np.zeros((batch_size,1))
        test_label_batch[0,0] = label[n_plot,0]  
        delta = scaler_1.inverse_transform(test_label_batch)
        
        d_test_image = spectra[n_plot,:,:,0:] 
        self.create_3D_plot(d_test_image[:,:,0])
        
        for i in range(x_points):
            d_test_label = np.array((i/x_points))
            image_batch_test[0,:] = d_test_image
            label_batch_test[0,:] = d_test_label
            d_pred = self.discriminator.predict([image_batch_test,label_batch_test])[0]
            d_params[i] = d_pred
        
        # Interval of Delta/t ratios of the training data
        x_min = 0.3
        x_max = 1.7
        x_grid = np.linspace(x_min, x_max, x_points)
        
        rescale_pred = np.argmax(d_params)/(x_points-1)
        rescale_pred = rescale_pred.reshape(-1,1)
        rescale_pred = scaler_1.inverse_transform(rescale_pred)
        
        fig, ax = plt.subplots(dpi=150)
        ax.plot(x_grid, d_params)
        # ax.set_ylim(min(d_params),max(d_params)+0.1)
        ax.set_xlim(x_min,x_max)
        plt.axvline(x=rescale_pred[0,0],color='purple', ls='--', lw=1.5,
                    label='max_pred = {:.2f}'.format(rescale_pred[0,0]))
        plt.axvline(x=scaler_1.inverse_transform(test_label_batch)[0,0],color='green', ls=':', lw=1.5, 
                    label='max_real= {:.2f}'.format(scaler_1.inverse_transform(test_label_batch)[0,0]))
        plt.legend()
        plt.xlabel("Delta/t-ratio")
        plt.ylabel("D-rating")
        plt.show()
        
        print('Delta/t (predicted) =',  np.argmax(d_params)/(x_points-1))
        print('Delta/t(scaled) =',  label[n_plot,0])             
        return None 



    def data_2D_to_3D(self, data_2D):
        """ convert 2D (input shape) to 3D data (plot shape)"""        
        data_3D = np.zeros((len(data_2D), self.pixels, self.pixels))
        for k in range(len(data_2D)):
            for j in range(self.pixels):
               for i in range(self.pixels):
                   data_3D[k,i,j]=data_2D[k,j*self.pixels+i]                        
        return data_3D


        
    def data_3D_to_2D(self, data_3D):
        """ convert 3D (plot shape) 2D data (input shape)"""       
        n_data = len(data_3D)
        data_2D = np.zeros((n_data, self.pixels*self.pixels))        
        for k in range(n_data):
            for m in range(self.pixels):
                for l in range(self.pixels):
                    data_2D[k,m*self.pixels+l] = data_3D[k,l,m]                     
        return data_2D

    
            
    def create_3D_plot(self, spectra, cmap='OrRd'):
        """ create 3D plot of data """
        matplotlib.rcParams['font.family'] = "Bitstream Vera Serif"
        fig = plt.figure(dpi=115, figsize=[4,4])
        plt.contourf(self.xs, self.ys, spectra, 100, cmap=cmap)
        plt.ylabel("muR")
        plt.xlabel("muL")
        plt.show()   
        return None
    
    
    
    def cut_image(self, data_exp, x_shift, y_shift, high_res=51, plot_images=True):
        """reduce (experimental) image size to 28x28 pixels.
            Shift center position of cut:
            x_shift: bigger -> left
            y_shift: bigger -> down"""
            
        # initialize data arrays
        data_cut = np.zeros((1,self.pixels,self.pixels,1))        
        data_exp_3D = np.zeros((high_res,high_res))

        for j in range(high_res):
            for i in range(high_res):
                data_exp_3D[j,i]=data_exp[j*high_res+i]
            
        data_cut[0,:,:,0] = data_exp_3D[(y_shift-self.pixels):y_shift, x_shift:(self.pixels+x_shift)]

        if plot_images == True:
            # high resolution
            muL_vals = np.linspace(-1, 1, high_res) # on-site energies for left quantum dot
            muR_vals = np.linspace(-1, 1, high_res) # on-site energies for right quantum dot
                          
            matplotlib.rcParams['font.family'] = "Bitstream Vera Serif"
            fig = plt.figure(dpi=115, figsize=[4,4])
            plt.contourf(muL_vals,muR_vals, data_exp_3D, 100, cmap='OrRd')
            plt.ylabel("muR")
            plt.xlabel("muL")
            plt.show()
            
            # low resolution
            muL_vals_2 = np.linspace(0, 1, self.pixels) # on-site energies for left quantum dot
            muR_vals_2 = np.linspace(0, 1, self.pixels) # on-site energies for right quantum dot
                    
            matplotlib.rcParams['font.family'] = "Bitstream Vera Serif"
            fig = plt.figure(dpi=115, figsize=[4,4])
            plt.contourf(muL_vals_2,muR_vals_2, data_cut[0,:,:,0], 100, cmap='OrRd')
            plt.axvline(x=0.5,color='red', ls='--', lw=1.0)
            plt.axhline(y=0.5,color='red', ls='--', lw=1.0)
            plt.ylabel("muR")
            plt.xlabel("muL")
            plt.show()
            
            print('shift center left: increase "x_shift"')
            print('shift center down: increase "y_shift"')      
        return data_cut



    def cut_image_3D(self, data_exp, x_shift, y_shift, high_res=51, plot_images=True):
        """reduce (experimental) image size to 28x28 pixels.
            Shift center position of cut:
            x_shift: bigger -> left
            y_shift: bigger -> down"""
        # initialize data arrays
        data_cut = np.zeros((1,self.pixels,self.pixels,1))        
        data_exp_3D = data_exp
        data_cut[0,:,:,0] = data_exp_3D[(y_shift-self.pixels):y_shift, x_shift:(self.pixels+x_shift)]

        if plot_images == True:
            # high resolution
            muL_vals = np.linspace(-1, 1, high_res) # on-site energies for left quantum dot
            muR_vals = np.linspace(-1, 1, high_res) # on-site energies for right quantum dot
                          
            matplotlib.rcParams['font.family'] = "Bitstream Vera Serif"
            fig = plt.figure(dpi=115, figsize=[4,4])
            plt.contourf(muL_vals,muR_vals, data_exp_3D, 100, cmap='OrRd')
            plt.ylabel("muR")
            plt.xlabel("muL")
            plt.show()
            
            # low resolution
            muL_vals_2 = np.linspace(0, 1, self.pixels) # on-site energies for left quantum dot
            muR_vals_2 = np.linspace(0, 1, self.pixels) # on-site energies for right quantum dot
                    
            matplotlib.rcParams['font.family'] = "Bitstream Vera Serif"
            fig = plt.figure(dpi=115, figsize=[4,4])
            plt.contourf(muL_vals_2,muR_vals_2, data_cut[0,:,:,0], 100, cmap='OrRd')
            plt.axvline(x=0.5,color='red', ls='--', lw=1.0)
            plt.axhline(y=0.5,color='red', ls='--', lw=1.0)
            plt.ylabel("muR")
            plt.xlabel("muL")
            plt.show()
            
            print('shift center left: increase "x_shift"')
            print('shift center down: increase "y_shift"')      
        return data_cut



    def scale_exp_data(self, data_exp, scale_max=0.87, scale_grad=1.0):
        """ scale experimental measurements to match training data.
            scale_max: changes maximum intensity of the image 
            scale_grad: changes gradient of intensity decay ~ noise level """
        return data_exp[:,:,:,:]**scale_grad * (1/np.amax(data_exp)**scale_grad) * scale_max
   
    
            
    def experimental_data(self, spectra, label, scaler_1, n_plot=0, x_points=50, batch_size=128, plot_orig=True):
        "estimate Hamiltonian parameters of experimental measurement"        
        # initialize
        d_params = np.zeros(x_points)
        batch = np.random.randint(0, spectra.shape[0], size=batch_size)
        image_batch_test = spectra[batch]
        label_batch_test = label[batch]
        
        # feed new data
        test_label_batch = np.zeros((batch_size,1))
        test_label_batch[0,0] = label[n_plot]         
        d_test_image = spectra[0,:,:,0:] 
        
        # decides if you're plotting the input image
        if plot_orig == True:
            self.create_3D_plot(d_test_image[:,:,0])
        
        for i in range(x_points):
            d_test_label = np.array((i/x_points))
            image_batch_test[0,:] = d_test_image
            label_batch_test[0] = d_test_label
            d_pred = self.discriminator.predict([image_batch_test,label_batch_test])[0]
            d_params[i] = d_pred
        
        # Interval of Delta/t ratios of the training data
        x_min = 0.3
        x_max = 1.7
        x_grid = np.linspace(x_min, x_max, x_points)
        
        rescale_pred = np.argmax(d_params)/(x_points-1)
        rescale_pred = rescale_pred.reshape(-1,1)
        rescale_pred = scaler_1.inverse_transform(rescale_pred)
        
        fig, ax = plt.subplots(dpi=100)
        ax.plot(x_grid, d_params, lw=2)
        ax.set_xlim(x_min,x_max)
        plt.axvline(x=rescale_pred[0,0],color='purple', ls='--', lw=2,
                    label='max_pred = {:.2f}'.format(rescale_pred[0,0]))
        plt.axvline(x=1/label[n_plot],color='green', ls='--', lw=2,
                    label='measuered = {:.2f}'.format(1/label[n_plot]))
        plt.legend()
        plt.xlabel("Delta/t-ratio")
        plt.ylabel("D-rating")
        plt.show()       
        return None    



    def experimental_data_max_pred(self, spectra, label, scaler_1, n_plot=0, x_points=25,
                                   batch_size=128, plot_orig=True, plot_pred=True):
        """estimate Hamiltonian parameters of experimental measurement and
           returns the maximal predicted value (rescaled) """        
        # initialize
        d_params = np.zeros(x_points)
        batch = np.random.randint(0, spectra.shape[0], size=batch_size)
        image_batch_test = spectra[batch]
        label_batch_test = label[batch]
        
        # feed new data
        test_label_batch = np.zeros((batch_size,1))
        test_label_batch[0,0] = label[n_plot]         
        d_test_image = spectra[0,:,:,0:] 
        
        # decides if you're plotting the input image
        if plot_orig == True:
            self.create_3D_plot(d_test_image[:,:,0])
        
        for i in range(x_points):
            d_test_label = np.array((i/x_points))
            image_batch_test[0,:] = d_test_image
            label_batch_test[0] = d_test_label
            d_pred = self.discriminator.predict([image_batch_test,label_batch_test])[0]
            d_params[i] = d_pred
        
        # Interval of Delta/t ratios of the training data
        x_min = 0.3
        x_max = 1.7
        x_grid = np.linspace(x_min, x_max, x_points)
        
        rescale_pred = np.argmax(d_params)/(x_points-1)
        rescale_pred = rescale_pred.reshape(-1,1)
        rescale_pred = scaler_1.inverse_transform(rescale_pred)
        
        # decides if you're plotting the prediction distribution
        if plot_pred == True:
            fig, ax = plt.subplots(dpi=100)
            ax.plot(x_grid, d_params, lw=2)
            ax.set_xlim(x_min,x_max)
            plt.axvline(x=rescale_pred[0,0],color='purple', ls='--', lw=2,
                        label='max_pred = {:.2f}'.format(rescale_pred[0,0]))
            plt.axvline(x=1/label[n_plot],color='green', ls='--', lw=2,
                        label='measuered = {:.2f}'.format(1/label[n_plot]))
            plt.legend()
            plt.xlabel("Delta/t-ratio")
            plt.ylabel("D-rating")
            plt.show()       
        return rescale_pred[0,0]
    
    
    
    def experimental_data_all_pred(self, spectra, label, scaler_1, n_plot=0, x_points=25,
                                   batch_size=128, plot_orig=True, plot_pred=True):
        """estimate Hamiltonian parameters of experimental measurement and
           returns the array of predicted values """  
        # initialize
        d_params = np.zeros(x_points)
        batch = np.random.randint(0, spectra.shape[0], size=batch_size)
        image_batch_test = spectra[batch]
        label_batch_test = label[batch]
        
        # feed new data
        test_label_batch = np.zeros((batch_size,1))
        test_label_batch[0,0] = label[n_plot]         
        d_test_image = spectra[0,:,:,0:] 
        
        # decides if you're plotting the input image
        if plot_orig == True:
            self.create_3D_plot(d_test_image[:,:,0])
        
        for i in range(x_points):
            d_test_label = np.array((i/x_points))
            image_batch_test[0,:] = d_test_image
            label_batch_test[0] = d_test_label
            d_pred = self.discriminator.predict([image_batch_test,label_batch_test])[0]
            d_params[i] = d_pred
        
        # Interval of Delta/t ratios of the training data
        x_min = 0.3
        x_max = 1.7
        x_grid = np.linspace(x_min, x_max, x_points)
        
        rescale_pred = np.argmax(d_params)/(x_points-1)
        rescale_pred = rescale_pred.reshape(-1,1)
        rescale_pred = scaler_1.inverse_transform(rescale_pred)
        
        # decides if you're plotting the prediction distribution
        if plot_pred == True:
            fig, ax = plt.subplots(dpi=100)
            ax.plot(x_grid, d_params, lw=2)
            ax.set_xlim(x_min,x_max)
            plt.axvline(x=rescale_pred[0,0],color='purple', ls='--', lw=2,
                        label='max_pred = {:.2f}'.format(rescale_pred[0,0]))
            plt.axvline(x=1/label[n_plot],color='green', ls='--', lw=2,
                        label='measuered = {:.2f}'.format(1/label[n_plot]))
            plt.legend()
            plt.xlabel("Delta/t-ratio")
            plt.ylabel("D-rating")
            plt.show()       
        return d_params 
    
    
    
    def fit_scaler(self, scaler, data):
        """ fit only one specific scaler """
        scaler.fit(data)
        return scaler 
    
    
    
    def set_scaler(self):
        """ initialize scaler and scaler_1 """
        scaler = MinMaxScaler(feature_range=(0,1)) # data
        scaler_1 = MinMaxScaler(feature_range=(0,1)) # label 1
        return scaler, scaler_1
    
        