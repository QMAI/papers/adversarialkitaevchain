# Adversarial Hamiltonian learning of quantum dots in a minimal Kitaev chain

### arXiv reference:

Supplemenraty code for the publication "Adversarial Hamiltonian learning of quantum dots in a minimal Kitaev chain" by Rouven Koch,David van Driel, Alberto Bordin, Jose L. Lado, and Eliska Greplova.

In this work, we use a convolutional conditional generative adversarial model (Conv-cGAN) to predict the Hamiltonian of a 2-quantum-dot system based on the Kitaev model and for the application to experimental differential conductance data measured with a InSb nanowire setup.

The goal is to tune the system into the "Poor Man's Majorana sweet-spot" by inferring the underlying Hamiltonian from measurement data.


## Instructions: 

The Conv-cGAN is defined in the script "Conv_cGAN.py" and the supervised CNN is defined in "CNN.py". Both scripts are written in Python, using Tensorflow and Keras for the Machine Learning/Deep Learning architecture. The required code versions can be found below.

Two jupyter-notebooks import the Conv-cGAN and CNN to produce the results presented in the manuscript.
        
- "experimental_predictions.ipynb" uses the ML algorithms to predict the underlying Hamiltonians and to generate new samples with the generator of the Conv-cGAN

- "plotting.ipynb" is the script that was used to create Figs. 3, 7, and 9 from the manuscript

The folders "data_cGAN" and "data_CNN" contain the pre-trained model parameter.

The folder "results" contains the predictions of the Conv-cGAN and CNN for the experimental data and the predictions of the Conv-cGAN for the simulated test set.

The folder "measurement_data" contails the local differential conductance measurements with the corresponding Delta/t ratio (Hamiltonian parameter)

## Code versions:

Python 3.8.3

Tensorflow 2.4.1 

Keras 2.4.0


### Last updated: Rouven Koch, 20th of April 2023

### Contact: rouven.koch@aalto.fi
